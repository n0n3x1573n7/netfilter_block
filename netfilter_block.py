from sys import argv

from blocker import *

def main(argv):
    def usage():
        print("python3 netfilter_block.py <host>")
        print("ex : python3 netfilter_block.py test.gilgil.net")

    try:
        name, host=argv
    except:
        usage()
        exit()

    set_iptables()
    netfilter_block(host)


if __name__ == '__main__':
    main(argv)