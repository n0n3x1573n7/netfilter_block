from atexit import register
from subprocess import Popen, PIPE

'''installed with:
apt-get install build-essential python-dev libnetfilter-queue-dev
pip install --upgrade python-iptables NetfilterQueue
'''
import iptc
from netfilterqueue import NetfilterQueue

from packet import IPv4_packet, TCP_packet

HOST=None

def set_iptables(*,use_popen=False):
    if use_popen:
        Popen('iptables -A OUTPUT -p tcp -j NFQUEUE --queue-num 0'.split(), stdout=PIPE)
        register(revert_iptables, use_popen=True)
    else:
        rule=iptc.Rule()
        rule.protocol="tcp"
        
        match=iptc.Match(rule, "tcp")

        rule.target=iptc.Target(rule, "NFQUEUE")

        chain=iptc.Chain(iptc.Table(iptc.Table.FILTER), "OUTPUT")
        chain.insert_rule(rule)

        register(revert_iptables, rule=rule, use_popen=False)

def revert_iptables(*, rule=None, use_popen=False):
    if use_popen:
        Popen('iptables -D OUTPUT -p tcp -j NFQUEUE --queue-num 0'.split(), stdout=PIPE)
    else:
        chain=iptc.Chain(iptc.Table(iptc.Table.FILTER), "OUTPUT")
        chain.delete_rule(rule)

def check_http(http_packet):
    ops='GET, POST, HEAD, PUT, DELETE, OPTIONS'.split(', ')
    for op in ops:
        if http_packet.startswith(op):
            return True
    return False

def packet_filtering(pkt):
    payload=pkt.get_payload()
    ip=IPv4_packet.analyze(payload)
    tcp=TCP_packet.analyze(ip.payload)
    try:
        http=tcp.data.decode().lstrip()

        if check_http(http):
            print(http)
            start=http.find('Host: ')
            end=http.find('\r\n', start)
            if http[start+len('Host: '):end]==HOST:
                pkt.drop()
    except Exception as e:
        if not str(e).startswith("'utf-8' codec"):
            print(e)
    pkt.accept()

def netfilter_block(host):
    global HOST
    HOST=host
    nfqueue=NetfilterQueue()
    nfqueue.bind(0, packet_filtering)
    try:
        nfqueue.run()
    except KeyboardInterrupt:
        print("Cleanup initiated")
        nfqueue.unbind()